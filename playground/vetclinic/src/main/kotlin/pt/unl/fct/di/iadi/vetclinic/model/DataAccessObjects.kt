/**
Copyright 2019 João Costa Seco

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package pt.unl.fct.di.iadi.vetclinic.model

import pt.unl.fct.di.iadi.vetclinic.api.AppointmentDTO
import pt.unl.fct.di.iadi.vetclinic.api.PetDTO
import pt.unl.fct.di.iadi.vetclinic.api.UserPasswordDTO
import java.util.*
import javax.persistence.*

@Entity
data class PetDAO(
        @Id @GeneratedValue val id:Long,
                            var name: String,
                            var species: String) {
        @OneToMany(mappedBy = "pet", cascade = [CascadeType.ALL])
                            var appointments:List<AppointmentDAO> = emptyList()

    constructor() : this(0,"","")

    constructor(pet: PetDTO, apts:List<AppointmentDAO>) : this(pet.id,pet.name,pet.species)

    fun update(other:PetDAO) {
        this.name = other.name
        this.species = other.species
        this.appointments = other.appointments
    }
}

@Entity
data class AppointmentDAO(
        @Id @GeneratedValue val id:Long,
                            var date: Date,
                            var desc:String,
        @ManyToOne          var pet:PetDAO
) {
    constructor() : this(0, Date(),"", PetDAO())
    constructor(apt: AppointmentDTO, pet:PetDAO) : this(apt.id, apt.date, apt.desc, pet)
}


@Entity
data class UserDAO(
        @Id
        val username: String = "",
        var password: String = "")
{
    constructor(user:UserPasswordDTO) : this(user.username, user.password)
}

data class UserDTO(
        val username: String)

