package pt.unl.fct.di.iadi.vetclinic.services

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import pt.unl.fct.di.iadi.vetclinic.model.UserDAO
import pt.unl.fct.di.iadi.vetclinic.model.UsersRepository
import java.util.*

@Service
class UsersService(var users: UsersRepository) {

    fun findUser(username:String): Optional<UserDAO> = users.findById(username)

    fun addUser(user: UserDAO) : Optional<UserDAO> {
        val aUser = users.findById(user.username)

        return if ( aUser.isPresent )
            Optional.empty()
        else {
            user.password = BCryptPasswordEncoder().encode(user.password)
            Optional.of(users.save(user))
        }
    }
}