package pt.unl.fct.di.iadi.vetclinic

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import pt.unl.fct.di.iadi.vetclinic.model.*
import java.util.*

@SpringBootApplication
class VetClinicApplication {

    @Bean()
    //@Profile("runtime") // to avoid conflicts with mocked repository components
    fun init(
            pets:PetRepository,
            apts: AppointmentRepository,
            users: UsersRepository
    ) =
        CommandLineRunner {
            val pantufas = PetDAO(1L, "pantufas", "dog")
            val bigodes = PetDAO(2L, "bigode", "cat")
            pets.saveAll(listOf(pantufas,bigodes))
            val consulta = AppointmentDAO(3L, Date(),"consulta", pantufas)
            apts.save(consulta)

            val user1 = UserDAO("user1",BCryptPasswordEncoder().encode("pwd"))
            val user2 = UserDAO("user2",BCryptPasswordEncoder().encode("pwd"))
            users.saveAll(listOf(user1,user2))
        }
}

fun main(args: Array<String>) {
    runApplication<VetClinicApplication>(*args)
}
