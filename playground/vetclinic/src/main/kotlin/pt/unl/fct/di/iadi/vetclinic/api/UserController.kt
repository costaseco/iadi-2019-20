package pt.unl.fct.di.iadi.vetclinic.api

import org.springframework.web.bind.annotation.*
import pt.unl.fct.di.iadi.vetclinic.model.UserDAO
import pt.unl.fct.di.iadi.vetclinic.model.UserDTO
import pt.unl.fct.di.iadi.vetclinic.services.UsersService

@RestController
@RequestMapping("")
class UserController(
        val users: UsersService
)
{
}