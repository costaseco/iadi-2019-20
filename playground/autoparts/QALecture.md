# Q&A Lecture: Sample questions

## Introduction

An Autoparts on-line shop that sells car parts and allows you to manage, list and search for an part for a vehicle. The system keeps a catalogue of all parts available and lets you search the parts given a starting reference to a car model. Notice that car parts can be listed for more than one model of vehicle. Car parts are categorized by type (given by a String).

## Questions
 
1. Design a RESTful interface that allows access to the available car models and corresponding parts. List all the endpoints by means of an Kotlin interface using data classes for DTO objects 



2. JPA modelling: Write two data classes for Carmodel and CarPart and relate them using JPA annotations



3. Define a repository for car parts and implement a custom JPQL query that filters car parts by car model and type.




4. Implement a controller method, and a service method that allows to access the car parts of a given car model and optionally filter them by type. 



5. Guarantee that only one query is performed when accessing all the parts in the previous search.


