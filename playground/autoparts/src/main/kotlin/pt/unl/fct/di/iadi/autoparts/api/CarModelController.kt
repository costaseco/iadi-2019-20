package pt.unl.fct.di.iadi.autoparts.api

import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.di.iadi.autoparts.services.PartService

@RestController
class CarModelController(val parts: PartService): CarModelAPI {
    override fun getAllModels(): Iterable<CarModelDTO> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addOneModel(part: CarModelDTO): Void {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getOneModel(id: Long): CarModelDTO {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateOneModel(id: Long, part: CarModelDTO): Void {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteOneModel(id: Long): Void {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPartsOfModel(id:Long, type:String?): Iterable<CarPartModelDTO> =
        //+ handle404
        (type?.let { parts.getPartsOfModelAndType(id,it) }
             ?: parts.getPartsOfModel(id))
             .map { CarPartModelDTO(CarPartDTO(it.id, it.name, it.type), CarModelDTO(it.cars[0].id, it.cars[0].name)) }

    override fun addCarToPart(id: Long, part: CarPartDTO): Void {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}