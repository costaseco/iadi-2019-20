package pt.unl.fct.di.iadi.autoparts.services

import org.springframework.stereotype.Service
import pt.unl.fct.di.iadi.autoparts.model.CarPartDAO
import pt.unl.fct.di.iadi.autoparts.model.CarPartsRepository
import java.util.*

@Service
class PartService(val parts: CarPartsRepository) {
    fun getPartsOfModelAndType(id: Long, type:String):Iterable<CarPartDAO> =
        parts.findByCarAndType(id, type)

    fun getPartsOfModel(id: Long): Iterable<CarPartDAO> = 
        parts.findByCar(id)
}