package pt.unl.fct.di.iadi.autoparts.api

import org.springframework.web.bind.annotation.*
import javax.websocket.server.PathParam

@RequestMapping("/cars")
interface CarModelAPI {

    @GetMapping("")
    fun getAllModels(): Iterable<CarModelDTO>

    @PostMapping("")
    fun addOneModel(@RequestBody part:CarModelDTO):Void
    // Maybe this does not make sense

    @GetMapping("/{id}")
    fun getOneModel(@PathVariable id:Long):CarModelDTO

    @PutMapping("/{id}")
    fun updateOneModel(@PathVariable id:Long, @RequestBody part:CarModelDTO):Void

    @DeleteMapping("/{id}")
    fun deleteOneModel(@PathVariable id:Long):Void

    @GetMapping("/{id}/parts")
    fun getPartsOfModel(@PathVariable id:Long,
                        @RequestParam(required = false) type:String?) : Iterable<CarPartModelDTO>

    @PostMapping("/{id}/parts")
    fun addCarToPart(@PathVariable id:Long, @RequestBody part:CarPartDTO):Void
}