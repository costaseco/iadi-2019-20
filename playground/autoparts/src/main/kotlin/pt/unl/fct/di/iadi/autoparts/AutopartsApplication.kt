package pt.unl.fct.di.iadi.autoparts

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import pt.unl.fct.di.iadi.autoparts.model.CarModelDAO
import pt.unl.fct.di.iadi.autoparts.model.CarModelRepository
import pt.unl.fct.di.iadi.autoparts.model.CarPartDAO
import pt.unl.fct.di.iadi.autoparts.model.CarPartsRepository

@SpringBootApplication
class AutopartsApplication {

    @Bean
    fun init(cars: CarModelRepository, parts: CarPartsRepository) : CommandLineRunner = CommandLineRunner {
        val boguinhas = CarModelDAO(0, "boguinhas", parts = emptyList())
        cars.save(boguinhas)

        val parachoque = CarPartDAO(0, "parachoque", "cromados", listOf<CarModelDAO>(boguinhas))
        parts.save(parachoque)
    }
}

fun main(args: Array<String>) {
    runApplication<AutopartsApplication>(*args)
}
