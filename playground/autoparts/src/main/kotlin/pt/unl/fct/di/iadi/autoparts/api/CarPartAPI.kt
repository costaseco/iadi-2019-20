package pt.unl.fct.di.iadi.autoparts.api

import org.springframework.web.bind.annotation.*

@RequestMapping("/parts")
interface CarPartAPI {

    @GetMapping("")
    fun getAllParts(): Iterable<CarPartDTO>

    @PostMapping("")
    fun addOnePart(@RequestBody part:CarPartModelDTO):Void
    // Maybe this does not make sense

    @GetMapping("/{id}")
    fun getOnePart(@PathVariable id:Long):CarPartDTO

    @PutMapping("/{id}")
    fun updateOnePart(@PathVariable id:Long, @RequestBody part:CarPartModelDTO):Void

    @DeleteMapping("/{id}")
    fun deleteOnePart(@PathVariable id:Long):Void

    @GetMapping("/{id}/cars")
    fun getCarsOfPart():Iterable<CarModelDTO>

    @PostMapping("/{id}/cars")
    fun addCarToPart(@PathVariable id:Long, @RequestBody car:CarModelDTO):Void
}